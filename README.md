[![pipeline status](https://gitlab.com/GeorgeMorosanu/sqm_project/badges/master/pipeline.svg)](https://gitlab.com/GeorgeMorosanu/sqm_project/-/commits/master)
[![coverage report](https://gitlab.com/GeorgeMorosanu/sqm_project/badges/master/coverage.svg)](https://gitlab.com/GeorgeMorosanu/sqm_project/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=GeorgeMorosanu_sqm_project&metric=alert_status)](https://sonarcloud.io/dashboard?id=GeorgeMorosanu_sqm_project)

Project name: JUnit 3
Website: 
	http://junit.sourceforge.net/junit3.8.1/
Original source code: 
	http://www.eclipse.org/jdt/ui/smoke-test/junit3.8.1src.zip
Automated build done with Maven:
  https://maven.apache.org/
  (not currently runnable due to lacking a main class as a JUnit 3 testing framework)
  Basic maven commands
    mvn install
      Installs package to local repository
    mvn compile
      compile the code into target directory
    mvn package
      package project and create JAR file
    mvn test
      test the mvn project with basic unit tests
      
Automated testing done with Junit:
    Can be run from test package as a Java program or from eclipse as Junit or maven test
    Automated testing can also be run troigh just maven with mvn test
  
Short description:
	JUnit is a simple framework to write repeatable tests. It is an instance of the xUnit architecture for unit testing frameworks.
	More details on the website :)
