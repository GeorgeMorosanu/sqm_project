import org.junit.Test;

public class AssertTest{

	// Test assertTrue
	@Test(expected = junit.framework.AssertionFailedError.class)
	public void testFail() {
		junit.framework.Assert.assertTrue("fail working", 5==2);
	}
	
	// Test assertFalse
	@Test(expected = junit.framework.AssertionFailedError.class)
	public void testFalseAssertionFails() {
		// the operation of "one"="one" will be true;
		// the message "assertFalse working correctly" is returned as a result of assertFalse
		// which implies that it works correctly
		junit.framework.Assert.assertFalse("assertFalse working correctly", "one"=="one");
	}
	
	@Test(expected = junit.framework.AssertionFailedError.class)
	public void testFailNull() {
		junit.framework.Assert.fail();
	}
}
